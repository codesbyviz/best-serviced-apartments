import Axios from 'axios';
import state from './state'
const instance = Axios.create({
    baseURL: state.api.endpoint,
    headers: {'Accept': 'application/json'}
  });
const actions = {
    GetApartments(){
        return new Promise((resolve,reject)=>{
            instance.get("apartments").then(res=>{resolve(res)}).catch(error=>{reject(error)})
        })        
    },
    GetApartment(context,id){
        return new Promise((resolve,reject)=>{
            instance.get("apartments/"+id).then(res=>{resolve(res)}).catch(error=>{reject(error)})
        })        
    },
}
export default actions;