import Vue from 'vue'
import App from './App.vue'
import Loader from './components/Loader.vue';
import router from './router'
import store from './store'
import './registerServiceWorker'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import './assets/scss/main.scss'
import * as VueGoogleMaps from 'vue2-google-maps'
const WOW = require('wowjs');
new WOW.WOW().init();


Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyD4MlrSlNrSa4gOnjEJE8Al1DSmZtrm6WE',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
 
    //// If you want to set the version, you can do so:
    // v: '3.26',
  },
 
  //// If you intend to programmatically custom event listener code
  //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  //// you might need to turn this on.
  // autobindAllEvents: false,
 
  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then disable the following:
  // installComponents: true,
})

Vue.use(require('vue-moment'));


Vue.use(Vuetify,{
  theme: {
    primary: '#0f172b',
    secondary: '#b0bec5',
    accent: '#8c9eff',
    error: '#b71c1c'
  }
})
Vue.config.productionTip = false
Vue.component('loader',Loader)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
