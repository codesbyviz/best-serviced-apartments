import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Contact from './views/Contact.vue'
import Apartments from './views/Apartments.vue'
import Apartment from './views/Apartment.vue'
import Booking from './views/Booking.vue'
import About from './views/About.vue'
import store from './store'
Vue.use(Router)

const router = new Router({
  mode:"history",
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta:{
        title:"Home"
      }
    },
    {
      path: '/about',
      name: 'About',
      component:About,
      meta:{
        title:"About Us"
      }      
    },
    {
      path: '/contact',
      name: 'Contact',
      component:Contact,
      meta:{
        title:"Get in Touch"
      }     
    },
    {
      path: '/apartments',
      name: 'Apartments',
      component:Apartments,
      meta:{
        title:"Apartments"
      }           
    },
    {
      path: '/apartment/:id',
      name: 'Apartment',
      component:Apartment,
      meta:{
        title:"Loading ..."
      }           
    },
    {
      path: '/apartment/:id/booking',
      name: 'Booking',
      component:Booking,
      meta:{
        title:"Booking"
      }           
    },
  ]
})
const DEFAULT_TITLE = store.state.app.name;

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || DEFAULT_TITLE;
  next();
})
export default router;