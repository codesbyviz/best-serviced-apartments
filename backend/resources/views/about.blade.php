@extends('layouts.app')

@section('title',"About")
@section('content')
<div class="jumbotron mb-0 bg-white text-black-50 border-radius-0">
    <div class="container">
        <h1 class="text-thin">About</h1>
    </div>
</div>
<section class="bg-white">
    <div class="container py-4 text-black-50">
        <p class="text-justify line-heighted">
        Best Serviced apartments offer a fine mix of a standard , superior and deluxe apartments
        to rent within mintiues in Technopark main areas .which are larger than hotel rooms, range from spacious Studios to 
        the ultimate in luxury, two 4 bedroom penthouses with private access.
    </p>
    <p class="text-justify line-heighted">
        This neo-classical style property originally functioned as four stylish
        townhouses in the 1870’s and today, each apartment has been designed to maximise light
        and space whilst retaining its period features. 
    </p>
    <p class="text-justify line-heighted">
        Each Serviced Apartment is elegantly
        furnished to provide comfortable, uncluttered accommodation for business travellers,
        leisure visitors and families. The apartments offer an ideal rental accommodation solution
        for short or long stays for the corporate or leisure visitor                        
    </p>

    <p class="text-justify line-heighted">
        Posted at the most happening part of Trivandrum city  is BEST Serviced Apartments with its immaculate
         services and a populous variety of hotel amenities. Our hospitality to guests is based on the comfort
          and values of perfection and we treasure the presence of every guest. At Best Serviced Apartments 
          we make sure that all our customers enjoy a unique homestay experience and make the best use of all
           the favor that we have to offer.
    </p>
    
    <p class="text-justify line-heighted">
        We have a passion for hospitality and property service which is reflected in our commitment to excellence. 
        Our idea and focus on quality service are passed down to the new employees from time to time. 
        We have earned the fruits of integrity, respect, and dedication to our hard efforts.
    </p>
</div>
</section>
@endsection
