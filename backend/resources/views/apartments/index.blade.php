@extends('layouts.app')

@section('title',"Home")
@section('content')
<div class="jumbotron border-radius-0 bg-white text-black mb-0 ">
    <div class="container text-center">
        <h1 class="">Our Apartments</h1>
        <p>Unwind the clock of modern life. Unlock the door to a wonder of the world.</p>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        @for ($i = 0; $i < $apartments->count(); $i++)
            @if ($i % 2 == 0)
            <div class="col-sm-12 col-md-8 bg-white doppleg" style="background-image:url({{Storage::url($apartments[$i]->image)}})"></div>            
            @endif
            <div class="col-sm-12 col-md-4 bg-primary text-white py-4">
                <div class="container p-4">
                    <div class="py-2"></div>
                    <h2>{{$apartments[$i]->name}}</h2>
                    <p>From</p>
                    <h3>Rs. {{$apartments[$i]->LowestPrice()->amount}}*</h3>
                <div class="py-2"></div>
                    <a href="{{route('apartments.show',$apartments[$i]->slug)}}" class="btn btn-outline-white border-radius-0">More Details <icon-item type="material" class="">arrow_forward</icon-item></a>
                    <a href="{{route('apartments.book',$apartments[$i]->slug)}}" class="btn btn-white border-radius-0">Book Now <icon-item type="material" class="">arrow_forward</icon-item></a>
                </div>
            </div>
            @if ($i % 2 !== 0)
                <div class="col-sm-12 col-md-8 bg-white doppleg" style="background-image:url({{Storage::url($apartments[$i]->image)}})"></div>            
            @endif            
        @endfor
        @empty($apartments)
            <div class="text-black-50 col-sm-12 text-center py-4">
                <icon-item type="fa" font-awesome-class="far fa-building fa-3x"></icon-item>
                <h4>No Apartments Available</h4>
            </div>
        @endempty
    </div>
</div>
@endsection
