@extends('layouts.app')

@section('title',"Book your Apartment")
@section('content')
    <booking apartment="{{$apartment}}"></booking>
@endsection
