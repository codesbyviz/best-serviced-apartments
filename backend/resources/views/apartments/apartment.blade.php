@extends('layouts.app')

@section('title',$apartment->name)
@section('content')
<section class="apartment-listing">
    <div class="jumbotron text-center bg-white text-black-50 mb-0">
        <div class="container">
            <h1 class="text-thin">{{$apartment->name}}</h1>
            <p class="">{{$apartment->subtitle}}</p>
        </div>
    </div>
    <section class="bg-white mb-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-9 text-justify p-4">
                                    <div class="p-4">
                                        {!!$apartment->description!!}
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-3 border-left p-4 bg-primary text-white">
                                        <p>From</p>
                                        <h3>Rs. {{$apartment->LowestPrice()->amount}}*</h3>
                                        <div class="py-2"></div>
                                        <a href="{{route('apartments.book',$apartment->slug)}}" class="btn btn-white btn-block border-radius-0">Book Now</a>                                    
                                    <div class="py-1"></div>
                                    @foreach ($apartment->Variation as $variation)
                                    <div class=" border-top">
                                        <table class="table text-white table-borderless">
                                            <tr>
                                                <th colspan="2" class="text-center">
                                                    <strong>{{$variation->name}}</strong><br/>
                                                    @if ($variation->Availablity() !== false)
                                                        <small class="text-success">Available</small>
                                                    @else
                                                        <small class="text-danger">Unavailable</small>
                                                    @endif
                                                </th>
                                            </tr>
                                            @if ($variation->Availablity())
                                            <tr>
                                                <th>
                                                    <icon-item type="material">check_circle_outline</icon-item>
                                                </th>
                                                <th class="text-right">
                                                    {{$variation->Availablity()->count()}} Apartments Vacant
                                                </th>
                                            </tr>
                                            @endif
                                            @if ($variation->Availablity())
                                            <tr>
                                                <th>
                                                    @if($variation->members == 1)  
                                                    <icon-item type="material" class="">person</icon-item>
                                                    @else 
                                                    <icon-item type="material" class="">group</icon-item>
                                                    @endif
                                                </th>
                                                <th class="text-right">
                                                    {{$variation->members}} Members Max.<br />
                                                </th>
                                            </tr>
                                            @endif
                                            <tr>
                                                <th colspan="2">
                                                    <h5 class="">Rs.{{$variation->amount}}*/Member</h5>
                                                </th>
                                            </tr>
                                        </table>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-primary p-4 text-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <h4>Amenities</h4>
                    <div class="py-1"></div>
                    <p>{!!$apartment->amenities!!}</p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <h4>Services Provided</h4>
                    <div class="py-1"></div>
                    <p>{!!$apartment->services!!}</p>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection
