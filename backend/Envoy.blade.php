@servers(['web' => 'localhost'])

@story('deploy')
    git
    composer
    php
@endstory

@task('git')
    git pull origin master
@endtask

@task('composer')
    composer install
@endtask

@task('install')
    php artisan key:generate
@endtask