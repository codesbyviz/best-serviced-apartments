<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
   public function Rooms()
   {
       return $this->hasMany('App\Room','variation','id');
   }
   public function Availablity()
   {
       $availablity = $this->Rooms()->where('status',1)->get();
       if($availablity->count() <= 0){
           return false;
        }
        else{
            return $availablity;
       }
   }
}
