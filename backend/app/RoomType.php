<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class RoomType extends Model
{
  protected $primaryKey = 'slug';
  public $incrementing = false;
  protected $hidden = [ 'image'];
  public $appends = ['image_url'];  

  public function Variation()
  {
      return $this->hasMany("App\Variation","room_type","id");
  }
  public function getImageUrlAttribute()
  {
    return Storage::disk(config('app.filesystem'))->url($this->image);
  }
  public function LowestPrice()
  {
      $amount = $this->Variation()->orderBy('amount',"asc")->first();
      if($amount){
        return $amount;
      }
      else{
        $am = new \App\Variation;
        $am->amount = 0;
        return $am;
      }
  }
}
