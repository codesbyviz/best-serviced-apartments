<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){})->name('home');
// Route::get('/about', 'HomeController@about')->name('about');
// Auth::routes();
// Route::resource('/app', 'PageController');
// Route::resource('/apartments', 'RoomTypeController');
// Route::get('/apartments/{id}/book', 'RoomTypeController@booking')->name('apartments.book');
