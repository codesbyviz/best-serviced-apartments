<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client as HttpClient;
use App\User;
class ApiController extends Controller
{
    public function Index()
    {
        return ["status"=>200,"message"=>"Welcome to Best Serviced Apartments API"];
    }
    public function Login(Request $request)
    {
        $http = new HttpClient;

        try {
            $response = $http->post(route('passport.token'),[
                'form_params' => [
                    'grant_type'    => "password",
                    'client_id'     => config("services.passport.key"),
                    'client_secret' => config("services.passport.secret"),
                    'username'      => $request->username,
                    'password'      => $request->password,
                ]
            ]);
            return $response->getBody();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if($e->getCode() == 400){
                return response()->json("Invalid Request. Please Enter a username and/or password $request->password",$e->getCode());
            }
            else if($e->getCode() == 401){
                return response()->json("Your username and/or password is incorrect",$e->getCode());
            }
            else{
                return response()->json("Something Went wrong",$e->getCode());
            }
        }
    }
    /*
    /*  Register via API . 
    */
    public function Register(Request $request)
    {
        // return $request;
        $request->validate([
            "name"=>"required|string|max:255",
            "email"=>"required|string|email|unique:users",
            "password"=>"required|string|min:6",
            "phone"=>"required|string|size:10",
            "country"=>"required|string",
        ],[
            'name.required' => 'Please Enter your Name',
            'email.required' => 'We need to know your e-mail address!',
            'email.unique' => 'This Email address is already registered. Please login',
            'password.required' => 'A strong password is required',
            'password.min' => 'Password Requires atleast 6 chars.',
            'phone.required' => 'Phone no is not valid. Please check ',
            'phone.size' => 'Phone no is not valid. Please check ',
            'country.required' => 'Please Select a Country',
        ]);
        
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->country = $request->country;
        $user->password = Hash::make($request->password);
        $user->phone = $request->phone;
        $user->save();
        return response()->json("Account Creation successful",200);
    }
    /*
    /*  Logout via API . 
    */
    public function Logout(Request $request)
    {
        return auth()->user()->tokens;
        auth()->user()->tokens->each(function($token,$key){
          $token->delete();
        });
      return response()->json("Logout Success",200);
    }    
}
