window.Vue = require('vue');
import Vuetify from 'vuetify'
import router from './router/router'
import store from './store/store'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'

import '../main.styl'
Vue.use(VueAwesomeSwiper, /* { default global options } */)
Vue.use(Vuetify,{
    theme: {
      primary: '#0f172b',
      secondary: '#b0bec5',
      accent: '#8c9eff',
      error: '#b71c1c'
    }
  })

Vue.component('App', require('./components/App.vue').default);
Vue.use(require('vue-moment'));
  const app = new Vue({
    el: '#app',
    router,
    store
});
