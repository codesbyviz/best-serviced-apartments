const state = {
    app:{
        name:"Best Serviced Apartments",
        version:1.2,
    },
    api:{
        key:process.env.VUE_APP_API_KEY,
        secret:process.env.VUE_APP_API_SECRET,
        endpoint:process.env.VUE_APP_API_ENDPOINT
    },
    
    
}
export default state;