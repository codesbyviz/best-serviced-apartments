import Vue from 'vue'
import Router from 'vue-router'
import store from './../store/store'
import routes from './routes'

Vue.use(Router)
const router = new Router({
  mode:"history",
  routes: routes,
})

const DEFAULT_TITLE = store.state.app.name;

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || DEFAULT_TITLE;
  next();
})
export default router;